from django.db import models

# A model with a list of Gods and their respective mythologies 
class Gods(models.Model):
    name    = models.CharField(max_length= 100)
    mythology   = models.CharField(max_length=100 , blank=False)
    
    def __str__(self):
        return self.name


