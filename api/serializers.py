from rest_framework import serializers
from .models import Gods

class GodsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gods
        field = '__all__'
        
