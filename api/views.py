from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Gods
from .serializers import GodsSerializer
# Create your views here.


@api_view(['GET'])
def getAll(request):
    data = Gods.objects.all()
    serializer = GodsSerializer(data , many= True)
    return Response(serializer.data)