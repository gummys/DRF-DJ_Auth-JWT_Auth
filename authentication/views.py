from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
# Create your views here.



def loginView(request):
    if request.user.is_authenticated:
        name = request.user.username
        print(name)
        context = {
            'username' : name,
            'message'   : 'success',
        }
        return render(request, 'user/profile.html',context)
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        if user is not None:
            login(request, user)
            
            name = request.user.username
            print(name)
            context = {
            'username' : name,
            'message'   : 'success',
            }
            return render(request, 'user/profile.html',context)
        else:
            name = request.user.username
            context = {
            'username' : name,
            'message'   : 'Failure',
            }
            return render(request, 'user/profile.html',context)
    else:
        return render(request, 'Registration/login.html')


def signupView(request):
    if request.user.is_authenticated:
        name = request.user.username
        context = {
            'username' : name,
            'message'   : 'success',
        }
        return render(request, 'user/profile.html',context)
    elif request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password1']
        if password != request.POST['password2']:
            raise("Mismatching passwords")
            return render(request, 'Registration/signup.html') 
        elif User.objects.filter(username=username).exists():
            raise("Choose Another name \nUsername already in use")
            return render(request, 'Registration/signup.html')
        else:
            user = User.objects.create_user(username=username,password=password)
            user.save()
            if user is not None:
                login(request, user)
                name = request.user.username
                context = {
                'username' : name,
                'message'   : 'success',
                }
                return render(request, 'user/profile.html',context)
            else:
                name = request.user.username
                context = {
                'username' : name,
                'message'   : 'Failure',
                }
                return render(request, 'user/profile.html',context)
    else:
        return render(request, 'Registration/signup.html')
