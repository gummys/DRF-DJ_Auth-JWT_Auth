from django.urls import path
from .views import *
urlpatterns = [
    path('', loginView, name='LoginView'),
    path('signup', signupView, name='SignupView'),
]